---
release_number: "11.9"
title: "GitLab 11.9がリリース。機密情報ファイルの検出、マージリクエストでの承認ルール"
author: Hiromi Nozawa
author_gitlab: hir0mi
image_title: '/images/11_9/11_9-cover-image.jpg'
description: "GitLab 11.9がリリースされました。機密情報ファイルの検出、マージリクエストでの承認ルール、ChatOpsをCoreプランへ移動など様々な点が改善がされています。"
twitter_image: '/images/tweets/gitlab-11-9-released.png'
categories: releases
layout: release
featured: yes
header_layout_dark: true
original_url: https://about.gitlab.com/2019/03/22/gitlab-11-9-released/
---

### 機密情報が漏洩したかどうかを迅速に把握

認証情報を共有リポジトリに誤ってコミットすると、重大な結果を招く可能性がありますが、これは単純なミスです。攻撃者はパスワードや API キーを一旦入手すると、あなたのアカウントを乗っ取り、不正に課金することができます。
これは、乗っ取られたアカウントから他のアカウントへのアクセスされる、ドミノ効果につながることさえあります。リスクが非常に高いため、機密情報が漏洩した場合にはできるだけ早く知ることが最も重要です。

今回のリリースでは SAST 機能の一部として [機密情報の検出](#detect-secrets-and-credentials-in-the-repository) を導入しています。CI/CD ジョブで各コミットをスキャンし、機密情報が含まれていないことを確認します。
スキャンにより機密情報を検出した場合、開発者はマージリクエストで警告を受け、漏洩した資格情報を無効にして新しい資格情報を生成するためのアクションを迅速に実行できます。

### 適切な変更管理の実施

組織が成長し、より複雑になるにつれて、組織のさまざまな部署間で整合性を保つことが難しくなります。同時に、アプリケーションのユーザ数と収益が増加すると、不適切なコードやセキュアでないコードをマージした場合の影響も大きくなります。適切なレビュープロセスに従っていることをコードがマージされる前に確認することは、そうしない場合のリスクが非常に大きいため、多くの組織にとって難しい要件です。

GitLab 11.9 では [マージリクエストの承認ルール](#merge-request-approval-rules) を利用して、より高度な制御と機構を提供しています。
これまでは、必要な承認のために個人またはグループを指定できました (グループのどのメンバーでも承認を与えることができます)。現在は、マージリクエストに複数のルールを追加して、個々の承認者に個別に要求したり、特定のグループで複数の承認者を要求することもできます。さらに、承認ルールにコードオーナ機能が統合されているため、誰が承認すべきかを簡単にトラッキングできます。

これにより、組織では複雑な承認フローを実現することができます。課題、コード、パイプライン、監視データを可視化してアクセスできる GitLab の単一アプリケーションのシンプルさを維持しながら、決定を通知して承認を迅速化することができます。


GitLab.com では承認ルールが一時的に無効になっていましたが、[リグレッション](https://gitlab.com/gitlab-org/gitlab-ee/issues/10356) のため GitLab 11.9 ではデフォルトで有効になっていません。
{: .alert .alert-info}

### ChatOps がオープンソースになりました

GitLab ChatOps は強力な自動化ツールで、どんな CI/CD ジョブでも実行でき、Slack や Mattermost のようなチャットアプリから直接 CI/CD ジョブのステータスを受け取ることができます。GitLab 10.6 で最初にリリースされた ChatOps は GitLab Ultimate 向けの機能でした。GitLab 社の [製品戦略](https://about.gitlab.com/handbook/ceo/pricing/#the-likely-type-of-buyer-determines-what-features-go-in-what-tier) および [オープンソースへの取り組み](https://about.gitlab.com/company/stewardship/) の一環として、上位プランの機能を下位のプランでも利用できるように定期的に見直しを実施しています。

ChatOps では、これは誰もが恩恵を受けられる機能であり、またこの機能自体がコミュニティからの貢献による恩恵を受けることができると判断しました。

GitLab 11.9 では [ChatOps をオープンソース化](#move-chatops-to-core) しているので GitLab セルフマネージド版の Core プランと GitLab.com SaaS版の Free プランで利用することができ、コミュニティへのコントリビューションも可能です。

### その他

このリリースでは、[フィーチャーフラグの監査](#auditing-for-feature-flags), [脆弱性修正のマージリクエスト](#vulnerability-remediation-merge-request), [セキュリティジョブ用の CI/CDテンプレート](#cicd-templates-for-security-jobs) など多くの優れた機能を利用できます。それら全ての機能を知るには、この記事をお読みください。
