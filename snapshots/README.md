# 機能一覧の更新手順

機能一覧の元ネタとなる `data/features.yml` は頻繁に更新されるので、計画的に追従する必要がある。
一ヶ月単位くらいでまとめて変更内容を適用する方針とし、そのために以下のような作業手順を定める。

1. www-gitlab-comリポジトリで、元ネタとするコミットを検索する
   * 2018年8月22日の最終コミットを探す例: `git log --before 2018-08-23 --first-parent`
1. 元ネタとなる`data/features.yml`を`snapshots/yyyymmdd-features.yml`にコピー
1. 前回からの差分を`snapshots/yyyymmdd-features.yml.diff`として保存
   * コマンド例: `diff -U 20 <previous features yaml> <current features yaml> > yyyymmdd-features.yml.diff`
1. `snapshots/README.md`に上記の情報を追記
1. `data/features.yml`に差分を手動マージ
   * 変更内容が大きい場合は、コメントアウトして`TODO:`マークを付ける

## 更新履歴

### features.yml

| 日付 | 元ネタ | ファイル | 差分 |
| ---- | -------------------------- | -------- | ---- |
| 2018-08-22 | [4000cb9](https://gitlab.com/gitlab-com/www-gitlab-com/tree/4000cb9) | [features.yml](snapshots/20180822-features.yml) | [diff](snapshots/20180822-features.yml.diff) |
| 2018-07-22 | [bc9087f](https://gitlab.com/gitlab-com/www-gitlab-com/tree/bc9087f) | [features.yml](snapshots/20180722-features.yml) | [diff](snapshots/20180722-features.yml.diff) |
| 2018-06-19 | [972f683](https://gitlab.com/gitlab-com/www-gitlab-com/tree/972f683) | [features.yml](snapshots/20180619-features.yml) | - |
